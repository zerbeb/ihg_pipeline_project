    This application stores objects as csv files for later use.  This is similar to packages like sql3lite, but since our sys-admin was against installing such packages, I wrote this.  This is at the heart of the data_mangagement.py script since it permanently stores the objects for continued use.  It reads objects created in the models.py file in directories that are supplied to mockdb from the system configuration file.

    Attributes can be freely added to objects.  Previous instances of the objects will simply record None for this attribute.
