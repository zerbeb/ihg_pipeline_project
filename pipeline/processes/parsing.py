import os
import sys
import re
import csv
import argparse

def parse_sequencing_run_dir(directory):
    base_dir = get_sequencing_run_base_dir(directory)
    (head,tail) = os.path.split(base_dir)
    names = tail.split("_")
    if len(names) < 4:
        raise Exception("Improper directory name {0}.".format(directory))
    date = names[0]
    machine = names[1]
    run_number = names[2]
    side = names[3][0:1]
    flowcell = names[3][1:]
    return [date,machine,run_number,side,flowcell]

def get_sequencing_run_base_dir(directory):
    (head,tail) = os.path.split(re.sub("/$","",directory))
    if re.search("Sample",tail):
        (head,tail) = os.path.split(head)
    if re.search("Project",tail):
        (head,tail) = os.path.split(head)
    if re.search("Basecall_Stats",tail):
        (head,tail) = os.path.split(head)
    return os.path.join(head,tail)

def parse_description_into_dictionary(description):
    """
    We use the description in the sample sheet to pass information to later steps.  The format
    is key1__value1--key2__value2--...  This function parses the description and returns a dict
    of type { key1: value1, key2: value2, ...}
    
    Later, I also added the ability of a key pointing to a list.  This has the format 
    keyi__valuei1~~valuei2~~valuei3~~... and would be a dict elemet like
    {keyi: [valuei1,valuei2,valuei3,...].
    """
    out_dict = {}
    pieces = description.split('--')
    for piece in pieces:
        content = piece.split('__')
        if len(content) == 2:  #Not everything in the description meets the dictionary format.
            possible_list = content[1].split("~~")
            if len(possible_list) > 1:
                out_dict.update({content[0]: possible_list})
            else:
                out_dict.update({content[0]: content[1]})
    return out_dict

def parse_description_string_portion(description):
    """
    Returns the portion of the string that does not match the dictionary format.
    """
    out_list = []
    pieces = description.split('--')
    for piece in pieces:
        content = piece.split('__')
        if len(content) != 2:  #Not everything in the description meets the dictionary format.  This pulls out this portion
            out_list.append(piece)
    return "--".join(out_list)

def write_dictionary_into_description_string(dictionary,front_string=None):
    """
    Does the reverse of parse_description_into_dictionary returning a dictionary and an optional
    front string to the --,__,~~ encoding format.
    """
    out_string = ""
    dict_list = []
    if not front_string is None and front_string != "":
        dict_list.append(front_string)
    for key, value in dictionary.iteritems():
        if isinstance(value,list):
            value = "~~".join(value)
        dict_list.append(key+"__"+value)
    out_string = "--".join(dict_list)
    return out_string

def add_key_value_pair_to_description(description,key,value,add_type="list append"):
    """
    Takes the description string and adds the key value pair according to the format
    described in parse_description_into_dictionary.  add_type tells how the value will be
    treated (appending into a list or overwriting the string value).
    """
    front_string = parse_description_string_portion(description)
    description_dict = parse_description_into_dictionary(description)
    if value is None:
        try:
            description_dict.pop(key)
        except KeyError:
            pass
    elif add_type == "list append":
        if not key in description_dict:
            description_dict[key] = []
        if not isinstance(description_dict[key],list): #List is just in case there is one string.
            description_dict[key]=description_dict[key].split(',')
        description_dict[key].append(value) 
    elif add_type == "overwrite":
        description_dict[key] = value
    return write_dictionary_into_description_string(description_dict,front_string)

def check_description_is_in_dictionary_format(description):
    """
    Some descriptions should be entirely in the key1__value1--key2__value2--... format.  Returns
    True if the description is in this format only and False otherwise.
    """
    pieces = description.split('--')
    for piece in pieces:
        content = piece.split('__')
        if len(content) != 2:  #Not everything in the description meets the dictionary format.
            return False
    return True
    

if __name__ == '__main__':
    #Handle arguments
    parser = argparse.ArgumentParser(description='Test various functions in this package')
    parser.add_argument('--description', dest="description", type=str, help='Transforms a string into dictionary according to the agreed upon description format.',default=None)
    parser.add_argument('--append_key', dest='key', type=str, help='Adds the key to the description in the agreed upon format.  Needs to be used with --description and --append_values flags.', default=None)
    parser.add_argument('--append_values', dest='values', type=str, help='Adds the values, separated by a comma, to the description in the agreed upon format.  Needs to be used with the --description and --append_key flags.', default=None)
    parser.add_argument('--overwrite', dest='overwrite', action="store_true", help='Results in only the last value being added.  Needs to be used with the --description, --append_key, and --append_values flags.', default=False)

    args = parser.parse_args()
    if not args.description is None:
        if not args.key is None and not args.values is None:
            values = args.values.split(",")
            out_string = args.description
            for value in values:
                if args.overwrite:
                    out_string = add_key_value_pair_to_description(out_string,args.key,value,add_type="overwrite")
                else:
                    out_string = add_key_value_pair_to_description(out_string,args.key,value)
            print out_string
        else:
            print parse_description_into_dictionary(args.description)
