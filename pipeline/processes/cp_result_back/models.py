import os
import re
from config.scripts import MyConfigParser
from time import strftime, localtime
from physical_objects.hiseq.models import Sample
from processes.models import SampleQsubProcess
from processes.pipeline.bcbio_config_interaction import grab_yaml
from template.scripts import fill_template


class CpResultBack(SampleQsubProcess):
    """
    Runs the cp that uploads the bcbio_upload directory to the proper location.
    """

    def __init__(self,config,key=int(-1),pipeline_config=None,prev_step=None,process_name='cp',pipeline=None,**kwargs):
        if not prev_step is None:
            if pipeline_config is None:
                pipeline_config = MyConfigParser()
                pipeline_config.read(config.get('Pipeline',pipeline.obj_type))
            cp_input_dir = os.path.join(pipeline.output_dir,"results")
            if not os.path.isdir(cp_input_dir):
                cp_input_dir = pipeline.output_dir
            self.description = pipeline.description
            if not hasattr(pipeline,"pipeline_key") or pipeline.pipeline_key is None:
                output_subdir_name = pipeline_config.safe_get('Common_directories','output_subdir','ngv3')
            else:
                automation_parameters_config = MyConfigParser()
                automation_parameters_config.read(config.get("Filenames","automation_config"))
                output_subdir_name = automation_parameters_config.safe_get('Output_subdir',pipeline.pipeline_key)
            for directory in os.listdir(config.safe_get('Common_directories','casava_output')):
                if os.path.isdir(os.path.join(config.safe_get('Common_directories','casava_output'),directory)):
                    if directory.endswith(pipeline.flowcell_key):
                        cp_dir = os.path.join(config.safe_get('Common_directories','casava_output'),directory+"/Project_"+pipeline.project + "/Sample_"+pipeline.sample_key+"/"+output_subdir_name)
            if not os.path.exists(cp_dir):
                os.makedirs(cp_dir)
            self.cp_dir = cp_dir
            SampleQsubProcess.__init__(self,config,key=key,input_dir=cp_input_dir,output_dir=pipeline.output_dir,process_name=process_name,**kwargs)
            if self.sample_key is not None:
                self.md5_file = os.path.join(cp_dir,self.sample_key + "_exome_md5checksums.txt")
            else:
                self.md5_file = "exome_md5checksums.txt"

    def __launch__(self,config,node_list=None):
        """
        Throttles the launching process by controlling the number of nodes and the queue the jobs can go to.
        """
        if node_list is None:
            node_list = config.safe_get('Nodes','cp')
        SampleQsubProcess.__launch__(self,config,node_list=node_list,queue_name='single')
        return True

