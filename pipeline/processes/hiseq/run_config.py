import re
import os
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
import argparse
from my_utils.file_handling import MissingFileException

class RunConfig():
    """
    Stores information about a sequencing run.
    """

    def __init__(self,config_file):
        """
        Just the basic read attributes of type, length, and actual legnth.
        """
        self.first_cycle,self.last_cycle,self.total_cycles = self.__extract_cycles__(config_file)

    def __extract_cycles__(self,config_file):
        """
        Extracts the cycle information from config.xml
        """
        if not os.path.isfile(config_file):
            raise MissingFileException(config_file,"__extract_cycles__")
        output = []
        tree = ET.ElementTree(file=config_file)
        for elem in tree.iter(tag='Cycles'):
            output.append(int(elem.attrib["First"]))
            output.append(int(elem.attrib["Last"]))
            output.append(int(elem.attrib["Number"]))
            return output

if __name__ == '__main__':
    #Handle arguments
    parser = argparse.ArgumentParser(description='Test various functions in this package')
    parser.add_argument('--path', dest="path", type=str, help='Provides the path for the function to be tested.')
    parser.add_argument('--total_cycles', dest="total_cycles", action="store_true", default=False, help='Tests reading of the number of cycles.')
 
    args = parser.parse_args()
    run_config=RunConfig(args.path)
    if args.total_cycles:
        print run_config.total_cycles
