import re
import os
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET
import argparse
from my_utils.file_handling import MissingFileException

def report_stderr_cycle_warnings(stderr_file):
    if not os.path.isfile(stderr_file):
        raise MissingFileException(stderr_file,"report_stderr_cycle_warnings")
    warnings = []
    with open(stderr_file,'r') as f:
        for line in f:
            if re.search("use-base-mask length is",line) is None:
                continue
            if re.search("expected length from config file is",line) is None:
                continue
            line = line.strip()
            warnings.append(line)
    return warnings
        
def report_stderr_error(stderr_file):
    if not os.path.isfile(stderr_file):
        raise MissingFileException(stderr_file,"report_stderr_error")
    errormsg = []
    with open(stderr_file,'r') as f:
        for line in f:
            if re.search("Error",line) is None:
                continue
            line = line.strip()
            errormsg.append(line)
    return errormsg

if __name__ == '__main__':
    #Handle arguments
    parser = argparse.ArgumentParser(description='Test various functions in this package')
    parser.add_argument('stderr_file', type=str, help='Provides the path of the stderr file to examine.')
    parser.add_argument('--cycles', dest="cycles", action="store_true", default=False, help='Returns the lines reporting the cycle warnings in the stderr file.')
    parser.add_argument('--cycles_file', dest="cycles_file", action="store_true", default=False, help='Returns the stderr file if the file contains cycle warnings.')
    parser.add_argument('--error', dest="error", action="store_true", default=False, help='Returns the lines with error messages in the stderr file.') 
    args = parser.parse_args()
    if args.cycles:
        cycle_warnings = report_stderr_cycle_warnings(args.stderr_file)
        if len(cycle_warnings) > 0:
            for warning in cycle_warnings:
                print warning
    if args.cycles_file:
        cycle_warnings = report_stderr_cycle_warnings(args.stderr_file)
        if len(cycle_warnings) > 0:
            print args.stderr_file
    if args.error:
        error_msg = report_stderr_error(args.stderr_file)
        if len(error_msg) > 0:
	    for error in error_msg:
                print error
  
