import os
import sys
import re
import csv
import argparse

class SampleSheetDescriptionException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
    def __unicode__(self):
        return repr(self.value)

class SampleSheetDescription():
    """
    We use the description to encode information in dictionaries and arrays.
    This object provides that utility.
    """

    def __init__(self,description_string,parse=True):
        self.__set_full_string__(description_string)
        self.__set_front_string__("")
        self.__set_dictionary__({},True)
        if parse:
            self.__set_front_string__(self.__parse_description_string_into_string_portion__())
            self.__set_dictionary__(self.__parse_description_string_into_dictionary_portion__())

    def __set_full_string__(self,in_string):
        """
        Sets the full_string attribute to the in_string.
        """
        self.full_string = in_string

    def __get_full_string__(self):
        """
        Returns the full_string attribute.
        """
        return self.full_string

    def __set_front_string__(self,in_string):
        """
        Sets the front_string attribute to the in_string.
        """
        self.front_string = in_string

    def __get_front_string__(self):
        """
        Returns the front_string attribute.
        """
        return self.front_string

    def __set_dictionary__(self,in_dict,full_overwrite=False,add_type="overwrite",*args,**kwargs):
        """
        Sets the dictionary for the object by adding all of the key value pairs in
        in_dict.  full_overwrite = True will first reset the
        dictionary to an empty dictionary wiping all previous keys.
        """
        if full_overwrite:
            self.dictionary = {}
        for key, value in in_dict.iteritems():
            self.__add_key_value_pair__(key,value,add_type,*args,**kwargs)

    def __add_key_value_pair__(self,key,value,add_type="list append"):
        """
        Adds a key value pair to the dictionary.  If value is None, 
        the key is removed from the dictionary.  add_type tells how the value will be
        treated (appending into a list or overwriting the string value).  

        Note:  Only a single level of lists are allowed, so list_append and overwrite treat lists differently.
        That is, overwrite has the key reference the input list while list append extends the elements
        the already existing list with the elements of the input list.
        """
        if value is None:
            self.__remove_key__(key)
        elif add_type == "list append":
            if not key in self.dictionary:
                self.dictionary[key] = []
            elif not isinstance(self.dictionary[key],list): #If there is only a single value to a key, it is simply a string.  If we are appending, we need to convert to a list.
                self.dictionary[key]=self.dictionary[key].split(',')
            if isinstance(value,list):
                self.dictionary[key].extend(value)
            else:
                self.dictionary[key].append(value)
        elif add_type == "overwrite":
            self.dictionary[key] = value

    def __remove_key__(self,key):
        """
        Removes the key from the dictionary (if it exists).
        """
        try:
            self.dictionary.pop(key)
        except KeyError:
            pass


    def __get_dictionary_value__(self,key=None):
        """
        Standard access to the dictionary's values.
        """
        if key is None:
            return self.dictionary
        if not key in self.dictionary:
            raise SampleSheetDescriptionError("Referencing an un-defined key " + key + " in the description dictionary")
        return self.dictionary[key]
    

    def __parse_description_string_into_dictionary_portion__(self):
        """
        We use the description in the sample sheet to pass information to later steps.  The format
        is key1__value1--key2__value2--...  This function parses the description and returns a dict
        of type { key1: value1, key2: value2, ...}
        
        Later, I also added the ability of a key pointing to a list.  This has the format 
        keyi__valuei1~~valuei2~~valuei3~~... and would be a dict elemet like
        {keyi: [valuei1,valuei2,valuei3,...].
        """
        out_dict = {}
        pieces = self.full_string.split('--')
        for piece in pieces:
            content = piece.split('__')
            if len(content) == 2:  #Not everything in the description meets the dictionary format.
                possible_list = content[1].split("~~")
                if len(possible_list) > 1:
                    out_dict.update({content[0]: possible_list})
                else:
                    out_dict.update({content[0]: content[1]})
        return out_dict
    
    def __parse_description_string_into_string_portion__(self):
        """
        Returns the portion of the string that does not match the dictionary format.
        """
        out_list = []
        pieces = self.full_string.split('--')
        for piece in pieces:
            content = piece.split('__')
            if len(content) != 2:  #Not everything in the description meets the dictionary format.  This pulls out this portion
                out_list.append(piece)
        return "--".join(out_list)

    def __encode_full_string__(self, overwrite=True):
        """
        Does the reverse of the parsing function encoding the dictionary and the optional
        front string in the --,__,~~ format.  If overwrite is true, stores the returned
        string in the full_string attributre
        """
        dict_list = []
        if not self.front_string is None and self.front_string != "":
           dict_list.append(self.__get_front_string__())
        for key, value in self.dictionary.iteritems():
            if isinstance(value,list):
                value = "~~".join(value)
            dict_list.append(key+"__"+value)
        if overwrite:
            self.__set_full_string__("--".join(dict_list))
        return "--".join(dict_list)


    def __check_description_is_in_dictionary_format__(self):
        """
        Some descriptions should be entirely in the key1__value1--key2__value2--... format.  Returns
        True if the description is in this format only and False otherwise.
        """
        pieces = self.full_string.split('--')
        for piece in pieces:
            content = piece.split('__')
            if len(content) != 2:  #Not everything in the description meets the dictionary format.
                return False
        return True
    

if __name__ == '__main__':
    #Handle arguments
    ###Need to test.
    parser = argparse.ArgumentParser(description='Test various functions in this package')
    parser.add_argument('--description', dest="description", type=str, help='Transforms a string into dictionary according to the agreed upon description format.',default=None)
    parser.add_argument('--append_key', dest='key', type=str, help='Adds the key to the description in the agreed upon format.  Needs to be used with --description and --append_values flags.', default=None)
    parser.add_argument('--append_values', dest='values', type=str, help='Adds the values, separated by a comma, to the description in the agreed upon format.  Needs to be used with the --description and --append_key flags.', default=None)
    parser.add_argument('--overwrite', dest='overwrite', action="store_true", help='Results in only the last value being added.  Needs to be used with the --description, --append_key, and --append_values flags.', default=False)

    args = parser.parse_args()
    if not args.description is None:
        description = SampleSheetDescription(args.description)
        if not args.key is None and not args.values is None:
            print "Original = " + str(description.__get_full_string__())
            values = args.values.split(",")
            for value in values:
                if args.overwrite:
                    description.__add_key_value_pair__(args.key,value,add_type="overwrite")
                else:
                    description.__add_key_value_pair__(args.key,value)
            description.__encode_full_string__()
            print "New      = " + str(description.__get_full_string__())
        else:
            print str(description.__get_dictionary_value__())
