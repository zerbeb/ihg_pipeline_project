import argparse
from my_utils.file_handling import MissingFileException
from processes.hiseq.run_config import RunConfig
from processes.hiseq.seq_read import SeqReadSet

def check_cycles(run_config_path,run_parameter_path):
    """
    Verifies that the number of cycles is consistent
    between the run config and the run parameters files.
    """
    run_config=RunConfig(run_config_path)
    seq_read_set = SeqReadSet()
    seq_read_set.__inject_seq_reads_from_run_parameters__(run_parameter_path)
    total_cycles = 0
    for seq_read in seq_read_set.seq_reads:
        total_cycles += seq_read.length
    return run_config.total_cycles == total_cycles

if __name__ == '__main__':
    #Handle arguments
    parser = argparse.ArgumentParser(description='Test various functions in this functions in this folder that require multiple modules')
    parser.add_argument('run_config_path', type=str, help='The config.xml file in the $flowcell/Data/Intensities directory.')
    parser.add_argument('run_parameter_path', type=str, help='The runParameters.xml file in the $flowcell directory.')
    parser.add_argument('--check_cycles', dest='check_cycles', action='store_true',default=False,help='Runs the check cycles function that makes sure the summed lengths of the sequence reads is the same as the config\'s cycles.')

    args = parser.parse_args()
    if args.check_cycles:
        try:
            if not check_cycles(args.run_config_path,args.run_parameter_path):
                print "Cycles differ in " + args.run_config_path + " and " + args.run_parameter_path
        except MissingFileException, msg:
            print msg
