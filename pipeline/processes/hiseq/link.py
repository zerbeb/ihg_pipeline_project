import os
import argparse
import re
from my_utils.file_handling import prepend_to_file

def create_link_to_web_portal(link_txt_path,output_name,casava_output_dir):
    output_dir = os.path.join(casava_output_dir,output_name)
    for directory in os.listdir(output_dir):
        if not directory.startswith('Project_'):
            continue
        if not os.path.isdir(os.path.join(output_dir,directory)):
            continue
        project_name = re.sub("Project_","",directory)
        pieces = project_name.split("-")
        if len(pieces) < 2:
            continue
        output = [pieces[0]] #The user
        output.append(os.path.join(output_dir,directory))
        output.append(output_name+"_"+directory)
        line = "\t".join(output)
        prepend_to_file(link_txt_path,line)

if __name__ == '__main__':
    #Handle arguments
    parser = argparse.ArgumentParser(description='Test various functions in this package')
    parser.add_argument('--link', dest="link", type=str, help='Links the flowcell directory (you provide) to the web portal.', default=None)

    args = parser.parse_args()
    if not args.link is None:
        link_txt_path = '/coldstorage/fastq/link.txt'
        casava_output_dir = '/coldstorage/fastq'
        create_link_to_web_portal(link_txt_path,args.link,casava_output_dir)
