The pipeline and system level (like QsubProcess) objects are defined in this directory.  Also, scripts for dealing with transitions or handeling the pipeline objects are also stored here.  Finally, sub-directories contain the definition objects for specific tasks.

Sub-directories:

    clean:  Objects associated with removing data.

    cp_results_back:  Object associated with copying data at the end of a pipeline back to the original directory of a pipeline.

    dnanexus:  Objects associated with interacting with dna nexus

    flowcell_stats_reports:  Object associated with keeping track of all of the stats of a fully run flowcell.

    hiseq:  Objects associated with producing and analyzing the raw HiSeq output.  This includes the SequencingRun, Casava, and IndexReport objects among others

    pipeline: Objects associated with running the analysis pipeline including the Bcbio object as well as MarkDuplicates, IndelRealignment, and BwaMem objects among others.

    summary_stats:  Objects associated with scripts run after a pipeline.

    zcat:  Objects associated with copying data between locations.

 
