import os
import dxpy
import argparse

class DNANexusFileException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
    def __unicode__(self):
        return repr(self.value)

class DNANexusFile():
    """
    Handy object for storing the DNANexus file metadata locally.
    """

    def __init__(self,local_path=None,type=None,dx_folder=None,dx_filename=None,dx_properties={},metadata={}):
        self.__set_id__(None) #Unique dnanexus id returned for the file on their system.
        self.__set_local_path__(local_path)
        self.__set_type__(type)
        self.__set_dx_folder__(dx_folder) #The folder to which we are uploading.
        self.__set_dx_filename__(dx_filename)
        self.dx_properties = {}
        self.__copy_dx_properties__(dx_properties)
        self.metadata = {}
        self.__copy_metadata__(metadata)

    def __set_id__(self,dxid):
        self.dxid = dxid

    def __get_id__(self):
        return self.dxid

    def __set_local_path__(self,local_path):
        if not local_path is None and not os.path.isfile(local_path):
            raise DNANexusFileException("The local path must be a valid file.  Instead, we have:\n\t"+str(local_path))
        self.local_path = local_path

    def __get_local_path__(self):
        return self.local_path

    def __set_type__(self,type):
        self.type = type

    def __get_type__(self):
        return self.type

    def __set_dx_folder__(self,dx_folder):
        self.dx_folder = dx_folder

    def __get_dx_folder__(self):
        return self.dx_folder

    def __set_dx_filename__(self,dx_filename = None):
        if dx_filename is None and not self.__get_local_path__() is None:
            self.dx_filename = os.path.basename(self.__get_local_path__())
        else:
            seld.dx_filename = dx_filename

    def __get_dx_filename__(self):
        return self.dx_filename

    def __set_dx_property__(self,key,value):
        """
        This overwrites the key value pair provided 
        """
        self.dx_properties[key] = value

    def __copy_dx_properties__(self,dictionary):
        for key, value in dictionary.iteritems():
            self.__set_dx_property__(key,value)

    def __get_dx_properties__(self,key=None):
        """
        Returns the value or the entire metadatum dictionary.
        """
        if not key is None:
            return self.dx_properties[key]
        return self.dx_properties

    def __set_metadatum__(self,key,value):
        """
        This overwrites the key value pair provided 
        """
        self.metadata[key] = value

    def __copy_metadata__(self,dictionary):
        for key, value in dictionary.iteritems():
            self.__set_metadatum__(key,value)

    def __get_metadata__(self,key=None):
        """
        Returns the value or the entire metadatum dictionary.
        """
        if not key is None:
            return self.metadata[key]
        return self.metadata

    def __upload__(self,create_parents=True,report=True):
        """
        Wraps the dnanexus upload function return the file id for the uploaded_file
        Default parameters:
            in_create_parents --- tells DNANexus to create the appropriate path on their s
            in_properties --- a dictionary that is passed to DNANexus which will attach th
                              with the value dict["key"]
        """
        if report is True:
            print self.dx_properties
        dxfile=dxpy.upload_local_file(filename=self.__get_local_path__(), 
                                      name=self.__get_dx_filename__(),
                                      folder=self.__get_dx_folder__(), 
                                      parents=create_parents, 
                                      properties=self.dx_properties)
        dxid = dxfile.describe()['id']
        self.__set_id__(dxid)
        if report is True:
            print self.__get_local_path__() + ": dnanexus file id = " + dxid


class DNANexusFileList():
    """
    Handles, sorts, and filters the set of DNANexusFiles.
    """

    def __init__(self):
        self.list = []

    def __add_file__(self,dnanexus_file=None,*args,**kwargs):
        """
        Creates if necessary, adds to the list, and returns the file object
        """
        if dnanexus_file is None:
            dnanexus_file = DNANexusFile(*args,**kwargs)
        self.list.append(dnanexus_file)
        return dnanexus_file

    def __merge_file_list__(self,file_list):
        """
        Adds an existing file list to the current file list.
        """
        for file in file_list.list:
            self.__add_file__(file)

    def __get_files_of_type__(self,type):
        """
        Returns a list of all files of the given type
        """
        new_file_list = DNANexusFileList()
        for file in self.list:
            if file.__get_type__() == type:
                new_file_list.__add_file__(file)
        return new_file_list

    def __get_files_with_metadatum__(self,key,value):
        """
        Returns a list of all files with the given key value pair.
        """
        new_file_list = DNANexusFileList()
        for file in self.list:
            if file.__get_metadata__(key) == value:
                new_file_list.__add_file__(file)
        return new_file_list

    def __copy__(self):
        """
        Returns a second list with the same file object in it.  Note
        these are the same DNANexusFile's, not copied files.
        """
        new_file_list = DNANexusFileList()
        for file in self.list:
            new_file_list.__add_file__(file)
        return new_file_list
 
    def __upload__(self,type=None,sample_sheet_obj_list=None):
        """
        Upload all files (or just files of type) in the DNANexusFileList.  Record
        the dnanexus file ids into the sample sheet obj.
        """
        if type is None:
            upload_files = self.__copy__()
        else:
            upload_files = self.__get_files_of_type__(type)
        for file in upload_files.list:
            if file.__get_type__() is None:
                continue
            file.__upload__()
