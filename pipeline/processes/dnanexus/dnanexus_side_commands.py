import dxpy
import os
import re
import argparse

def parse_upload_stdout_for_file_ids(upload_stdout):
    """
    The DNANexus file ids are printed in the stdout of the upload script.
    This extracts those ids from the file.  This assumes a single flowcell.
    """
    file_ids = {}
    file_ids["sample_samplesheets"] = []
    file_ids["project_samplesheets"] = []
    with open(upload_stdout,'r') as f:
        for line in f:
            line = line.rstrip()
            if re.search(": dnanexus file id = ",line):
                pieces = line.split(": dnanexus file id = ")
                path = pieces[0]
                dxid = pieces[1]
                filename = os.path.basename(path)
                if filename == "run_qc_metrics.txt":
                    file_ids["run_qc_metrics"] = {"dxid":dxid,"local_path":path}
                elif filename == "SampleSheet.csv":
                    dirname = os.path.dirname(path)
                    last_dirname = os.path.basename(dirname)
                    if last_dirname.startswith("Sample_"):
                        file_ids["sample_samplesheets"].append({"dxid":dxid,"sample_name":last_dirname.replace("Sample_",""),"local_path":path})
                    if last_dirname.startswith("Project_"):
                        file_ids["project_samplesheets"].append({"dxid":dxid,"project_name":last_dirname.replace("Project_",""),"local_path":path})
    return file_ids

def submit_ccgl_dispatcher(samplesheet_dxid,metrics_dxid,output_dir,name_designation=None):
    """
    Submits a ccgl_pipeline return the dxid for the process.
    """
    args = {}
    args["sample_sheet"] = dxpy.dxlink(samplesheet_dxid)
    args["run_qc_metrics"] = dxpy.dxlink(metrics_dxid)
    name_list = ["CCGL Dispatcher"]
    if not name_designation is None:
        name_list.append(name_designation)
    my_app = dxpy.DXApp(name="ccgl_dispatcher")
    my_job = my_app.run(args,folder=output_dir,name=" - ".join(name_list))
    return my_job.describe()["id"]

def submit_dispatcher_from_upload_stdout(upload_stdout):
    """
    Extracts the file ids from the stdout and uses these to
    send jobs to ccgl_dispatcher.  This assumes a single flowcell.
    """
    file_ids = parse_upload_stdout_for_file_ids(upload_stdout)
    dispatcher_ids = []
    for samplesheet_file in file_ids["sample_samplesheets"]:
        dispatcher_ids.append(submit_ccgl_dispatcher(samplesheet_file["dxid"],file_ids["run_qc_metrics"]["dxid"]))
    return dispatcher_ids
    
if __name__ == '__main__':
    #Handle arguments
    parser = argparse.ArgumentParser(description='Test various functions in this package')
    parser.add_argument('--parse_upload_stdout', dest="upload_stdout", type=str, help='Parses the provided upload stdout from the dna nexus upload function for file ids.',default=None)
    parser.add_argument('--start_from_upload_stdout', dest="starting_upload_stdout", type=str, help='Parses the provided upload stdout from the dna nexus upload function for file ids and submits the samples to ccgl_dispatcher.',default=None)

    args = parser.parse_args()
    if not args.upload_stdout is None:
        print str(parse_upload_stdout_for_file_ids(args.upload_stdout))
    if not args.starting_upload_stdout is None:
        print str(submit_dispatcher_from_upload_stdout(args.starting_upload_stdout))
