import dxpy
import os
import csv
import argparse
from processes.dnanexus.dnanexus_side_commands import submit_ccgl_dispatcher
from processes.dnanexus.dx_file_object import DNANexusFileList
from processes.hiseq.scripts import extract_metadata_from_sample_dir, extract_metadata_from_fastq_file
from processes.hiseq.sample_sheet import SampleSheetObjList, SampleSheetFormatException
from processes.parsing import add_key_value_pair_to_description

gl_proj = []
gl_proj_name = 'CCGL_'
gl_sample_csv = 'SampleSheet.csv'

# Uploads files to DNA Nexus server.
# in_path - Traverses the path and reads all the files as per the sample csv
# in_run_metrics_path - path of the run metrics file to upload to dna nexus
def upload_files_to_dnanexus(in_path, in_run_metrics_path):
    """
    Wraps the uploading process returning the sample sheet and run
    qs metrics dnanexus files.
    """
    
    print "Initializing upload"
    initialize_upload(in_path)
    print "creating patient projects"
    create_proj_dir(in_path)
    print "Preparing files for upload"
    dnanexus_files = prepare_files_for_upload(in_path,in_run_metrics_path)
    print "Uploading files"
    dnanexus_files.__upload__("fastq")
    add_fastq_file_ids_to_sample_sheets(dnanexus_files)
    dnanexus_files.__upload__("SampleSheet")
    dnanexus_files.__upload__("project SampleSheet")
    dnanexus_files.__upload__("run qc metrics")
    return dnanexus_files

def add_fastq_file_ids_to_sample_sheets(dnanexus_files):
    """
    Adds the dxids from the fastq files to the sample sheet object, and
    then writes the sample sheets again.
    """
    sample_samplesheets = dnanexus_files.__get_files_of_type__("SampleSheet")
    project_samplesheets = dnanexus_files.__get_files_of_type__("project SampleSheet")
    fastqs = dnanexus_files.__get_files_of_type__("fastq")
    for project_samplesheet in project_samplesheets.list:
        full_sample_sheet_obj_list = SampleSheetObjList(sample_sheet_file=project_samplesheet.__get_local_path__())
        sample_sample_sheet_obj_list = full_sample_sheet_obj_list.__partition_sample_sheet_objects__("SampleID")
        lane_sample_sheet_obj_list = sample_sample_sheet_obj_list.__partition_sample_sheet_objects__("Lane") #This has been partitioned on both Lane and SampleID
        project_name = project_samplesheet.__get_metadata__("project name")
        project_fastqs = fastqs.__get_files_with_metadatum__("project name",project_name)
        #Edit the descriptions to include the fastq file dnanexus ids.
        for lane_sample_sheet_obj in lane_sample_sheet_obj_list.list:
            sample_name = lane_sample_sheet_obj.__get_meta_datum__("SampleID")
            lane = str(lane_sample_sheet_obj.__get_meta_datum__("Lane"))
            if len(lane_sample_sheet_obj.sample_sheet_table.rows) != 1:
                raise SampleSheetFormatException("One row was expected for " + sample_name + " in lane " + lane + " in the file " + project_sample_sheet.__get_local_path())
            description_index = lane_sample_sheet_obj.sample_sheet_table.__get_field_index__("Description")
            description = lane_sample_sheet_obj.sample_sheet_table.rows[0][description_index]
            sample_fastqs = fastqs.__get_files_with_metadatum__("sample name",sample_name)
            lane_sample_fastqs = sample_fastqs.__get_files_with_metadatum__("lane",lane)
            description = add_key_value_pair_to_description(description,"dxids",None)#Nulls it out so that older dxids are overwritten.
            for fastq in lane_sample_fastqs.list:
                description = add_key_value_pair_to_description(description,"dxids",fastq.__get_id__())
            lane_sample_sheet_obj.sample_sheet_table.rows[0][description_index] = description
        #Write the project_samplesheet again
        full_sample_sheet_obj_list.list[0].sample_sheet_table.__write_file__(project_samplesheet.__get_local_path__())
        """
        #Now write the individual sample sheet files to the sample directories.
        for sample_samplesheet in sample_samplesheets.__get_files_with_metadatum__("project name",project_name).list:
            sample_name = sample_samplesheet.__get_metadata__("sample name")
            specific_sample_sheet_obj_list = sample_sample_sheet_obj_list.__filter_sample_sheet_objects__({"SampleID":sample_name})
            if len(specific_sample_sheet_obj_list.list) != 1:
                raise SampleSheetFormatException("More than one sample sheet was found for " + sample_name )
            specific_sample_sheet_obj_list.list[0].sample_sheet_table.__write_file__(sample_samplesheet.__get_local_path__())
        """
    return


def prepare_files_for_upload(in_path, in_run_metrics_path):
    """
    Creates the DNANexusFile list and files in all the relavent data
    needed for uploading, etc.
    """
    dnanexus_files = DNANexusFileList()
    for directory in os.listdir(in_path):
        if os.path.isdir(os.path.join(in_path,directory)) and directory.startswith("Project_"):
            csv_path = os.path.join(os.path.join(in_path, directory), gl_sample_csv)
            #sample_sheet_obj_list = SampleSheetObjList(sample_sheet_file = csv_path)
            #projs = sample_sheet_obj_list.__get_column_values__("SampleProject") #List of all projects in the SampleSheet
            #sample_sheet_obj_list = sample_sheet_obj_list.__partition_sample_sheet_objects__("SampleProject") #SampleProject is now part of meta_data
            #sample_sheet_obj_list = sample_sheet_obj_list.__partition_sample_sheet_objects__("SampleID") #Working at the individual sample level
            #sample_sheet_obj_list = sample_sheet_obj_list.__partition_sample_sheet_objects__("Lane") #Break the samples all the way down to the lane level.
            for root, dirs, files in os.walk(os.path.join(in_path,directory)):    
                if 'Sample_' in root and 'Undetermined_' not in root:    
                    samplecsv = [f for f in files if f.endswith('.csv')]
                    # Go in only if there is csv file in either root or in sample folder
                    if samplecsv:
                        data = parseSampleCSV(os.path.join(root, samplecsv[0]), root)
                        create_output_directory(data[0][0])
                        samplesheet_dnanexus_files = prepare_samples_files(root, data, files)
                        dnanexus_files.__merge_file_list__(samplesheet_dnanexus_files)

            for proj in gl_proj:
                dnanexus_files.__add_file__(local_path=csv_path,
                                            type="project SampleSheet",
                                            dx_folder = '/' + proj,
                                            metadata={"project name": proj})
                dnanexus_files.__add_file__(local_path=in_run_metrics_path,
                                            type="run qc metrics",
                                            dx_folder = '/' + proj,
                                            metadata={"project name": proj})
    return dnanexus_files

def prepare_samples_files(in_root, in_data, in_files):
	"""
        Unpacks the data and passes it to the prep function.
        """
        dnanexus_files = DNANexusFileList()
	for item in in_data:
		item_dnanexus_files = prepare_files_in_sample_directory(item[0], in_files, in_root, item[1], item[2])
		dnanexus_files.__merge_file_list__(item_dnanexus_files)
        return dnanexus_files
				
def prepare_files_in_sample_directory(in_parent, in_files, in_root, in_fcid, in_properties):
    """
    Goes through the in_files list and 
    fills the appropriate properties into the DNANexusFileList.
    """
    sample_data = extract_metadata_from_sample_dir(in_root)
    dnanexus_files = DNANexusFileList()
    for file in in_files:
        dnanexus_file = dnanexus_files.__add_file__(local_path=os.path.join(in_root, file),dx_folder=in_parent)
        dnanexus_file.__copy_metadata__(sample_data)
        if file.endswith('.fastq.gz'):
            fastq_data = extract_metadata_from_fastq_file(file)
            dnanexus_file.__copy_metadata__(fastq_data)
            dnanexus_file.__set_type__("fastq")
            dnanexus_file.__copy_dx_properties__(in_properties)
        elif file.endswith('.csv'):
            dnanexus_file.__set_type__("SampleSheet")
    return dnanexus_files

def create_output_directory(in_parent_folder):
        """
        Creates the out directory in the sample directory.
        """
	projects = dxpy.api.system_find_projects(input_params={'name':'CCGL_RawData'})
        output_dir = os.path.join(os.path.dirname(in_parent_folder),"out")
        for project in projects["results"]:
       	        obj = dxpy.api.file_new({"project":project["id"],"folder":output_dir,"parents":True})
       	        dxpy.api.project_remove_objects(project["id"],{"objects":[obj["id"]]})


def parseSampleCSV(in_proj_path, in_sample_path):
	"""
        Does 2 things:
            1.  Adds SampleProject to gl_proj if the SampleProject
                is not already present. --- this is not necessary with 
                sample sheet obj list object.
            2.  Creates a list of triplets (DNANexus dir structure,
                Flowcell, DNANexus Dictionary) that is returned.
        """
	projData = []
	csvRowKey = [] # this structure holds csv project sample key to filter rows already added to projData

	with open(os.path.join(in_proj_path)) as f:
		reader = csv.reader(f)
		headers = reader.next()

		### gather all indexes from the csv files
		sample_proj_idx = headers.index('SampleProject')
		desc_idx = headers.index('Description')
		fcid_idx = headers.index('FCID')
		smpl_idx = headers.index('SampleID')

		for row in reader:	
			descdict = parse_description(row[desc_idx])
		  					
			if gl_proj_name in descdict['Pipeline']:
				rowKey = '/' + row[sample_proj_idx] + '/' + row[smpl_idx] 

				if not rowKey in csvRowKey:
					csvRowKey.append(rowKey)
					projData.append([rowKey, row[fcid_idx], descdict])

				if row[sample_proj_idx] not in gl_proj:
					gl_proj.append(row[sample_proj_idx])

			

	return projData

def parse_description(in_desc):
	"""
        DNANexus variables are encoded in the Description
        as a dictionary in the format with the format
        key1__value1--key2__value2--etc.  Unpacks this dictionary.
        """
	#print in_desc
	descdict = {}
	descarr = in_desc.split('--')

	for item in descarr:
		arr = item.split('__')
		descdict[arr[0]] = arr[1]

	return descdict

def initialize_upload(in_path):
	"""
        Create csv file in the Project Directory.  This Sample sheet has the --filecount__
        """
	proj = {}
	headerArr = []

	for root, dirs, files in os.walk(in_path):
		if 'Sample_' in root and not 'Undetermined_' in root:
			l1_count = 0
			l2_count = 0
			for f in files:
				if '.fastq.gz' in f:
					if 'L001' in f:
						l1_count += 1
					else:
						l2_count += 1

			csvfile = [f for f in files if f == gl_sample_csv]
			if len(csvfile) > 0:
				csv_data = []
				
				with open(os.path.join(root, csvfile[0])) as f1:
					reader = csv.reader(f1)					
					if len(headerArr) > 0:
						reader.next()
					
					for row in reader:
						if len(headerArr) == 0:
							headerArr.append(row)
							continue

						projName = 'Project_' + row[9]
						if not projName in proj:
							proj[projName] = []
						if len(headerArr) > 0 and row[1] == '1':
							row[5] += '--filecount__' + str(l1_count)
						elif len(headerArr) > 0 and row[1] == '2':
							row[5] += '--filecount__' + str(l2_count)
						validate_description(row[5])
						proj[projName].append(row)

	### Create csv file
	for key in proj.keys():
		path = os.path.join(in_path, key)
                if os.path.isfile(os.path.join(path, gl_sample_csv)): #Added in case we want to rerun the script.
                        pass
		else:
                        with open(os.path.join(path, gl_sample_csv), 'a') as f:
			        writer = csv.writer(f)
			        writer.writerow(headerArr[0])
			        writer.writerows(proj[key])

def create_proj_dir(in_path):
	"""
        Extract the DNANexus PatientID from the Descripion field in
        the SampleSheet file and create the patient ID folder
        on DNANexus if necessary.
        """
	patientdata = []

	for root,dirs,files in os.walk(in_path):
		basename = os.path.basename(root)

		if 'Project_' in basename:
			samplesheet = [f for f in files if f == gl_sample_csv]

			if len(samplesheet) > 0:
				filename = os.path.join(root, samplesheet[0])

				with open(os.path.join(in_path,filename)) as f:
					reader = csv.reader(f)
					reader.next() ## skip header
					for row in reader:
						desc_data = parse_description(row[5]) #5 is the Description column
						if not desc_data['PatientID'].startswith('RPA'):
							patientdata.append(desc_data['PatientID'])

	dataset = set(patientdata)
	for folname in dataset:
		#print dxpy.PROJECT_CONTEXT_ID
		#handler = dxpy.bindings.dxproject.DXProject(gl_patient_proj_context_id)
		#handler.new_folder('/'+folname)
		projects = dxpy.api.system_find_projects(input_params={'name':folname})
		if len(projects['results']) == 0:
			obj = dxpy.api.project_new(input_params={'name':folname})
			dxpy.api.project_invite(obj['id'],input_params={'invitee':'org-gmi_informatics','level':'CONTRIBUTE'})
			dxpy.api.project_invite(obj['id'],input_params={'invitee':'org-gmi_staff','level':'VIEW'})

#"""Validates description field for the proper format,if not valid format then raises exception"""
def validate_description(in_desc):
	itemarr = in_desc.split('--')

	for item in itemarr:
		try:
			key, value = item.split('__')
		except ValueError:
			raise ValueError("Invalid description text in" "sample sheet: %r" % itemarr)
				
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('path', help='Flowcell directory full path')
	parser.add_argument('metricsfilepath', help='Full path of run metrics file.')
#	parser.add_argument('--projectid', dest='projectid', default=None, help='project id of the desired Project on Dnanexus.')
	args = parser.parse_args()
	dnanexus_files = upload_files_to_dnanexus(args.path, args.metricsfilepath)
        sample_sheets = dnanexus_files.__get_files_of_type__("project SampleSheet")
        run_qc_metrics = dnanexus_files.__get_files_of_type__("run qc metrics")
        for sample_sheet_file in sample_sheets.list:
            project_name = sample_sheet_file.__get_metadata__("project name")
            name_designation = project_name[7:]
            project_run_qc_metrics = run_qc_metrics.__get_files_with_metadatum__("project name",project_name)
            if len(project_run_qc_metrics.list) != 1:
                raise Exception("Unexpected number of run qc metrics files for the given sample sheet. Found "+ str(len(project_run_qc_metrics.list)) +" expected 1.")
            print sample_sheet_file.__get_id__()
            print project_run_qc_metrics.list[0].__get_id__()
            print "project name = " + project_name + " : job_id = " + str(submit_ccgl_dispatcher(sample_sheet_file.__get_id__(),project_run_qc_metrics.list[0].__get_id__(),os.path.join(sample_sheet_file.__get_dx_folder__(),'out'),name_designation))
