import os
import argparse
import re
from config.scripts import MyConfigParser
from mockdb.initiate_mockdb import initiate_mockdb,save_mockdb
from processes.hiseq.scripts import check_fastq_output
from processes.parsing import parse_sequencing_run_dir

parser = argparse.ArgumentParser(description='Checks md5sum, fastqc, and index count of the provided directory, and runs the appropriate scripts if output is missing.')
parser.add_argument('paths', nargs='+', help='Full path of the flowcell directories to check and run.', default=None)
parser.add_argument('--system_config', dest='system_config_file', help='The system configuration file', default='/home/sequencing/src/pipeline_project/pipeline/config/ihg_system.cfg')
parser.add_argument('-d', '--debug', dest='debug', action='store_true', help='Turn debugging on', default=False)
parser.add_argument('--report', dest='report', action='store_true', help='Only prints out the details, does not try to fix issues.', default=False)
options = parser.parse_args()

system_config = MyConfigParser()
system_config.read(options.system_config_file)
system_config.add_section("Logging")
if options.debug is True:
    system_config.set("Logging","debug","True")
else:
    system_config.set("Logging","debug","False")

pipeline_config = MyConfigParser()
pipeline_config.add_section("Template_files")
pipeline_config.set("Template_files","fastqc", "fastqc_0.10.1.template")
pipeline_config.set("Template_files","md5_check_sum", "md5sum_fastq.template")
pipeline_config.set("Template_files","index_report", "index_report.template")

if not options.report:
  mockdb=initiate_mockdb(system_config)


for path in options.paths:
    fastq_output = check_fastq_output(path)
    if options.report:
        print str(fastq_output)
        continue
    if not fastq_output["sample_sheet"] is True:
        print fastq_output["sample_sheet"]
    if "index" in fastq_output and not fastq_output["index"] is True:
        undetermined_dir = os.path.join(path,"Undetermined_indices")
        if not os.path.isdir(undetermined_dir):
            raise Exception("The undetermined indices directory, " + undetermined_dir + " does not exist.\n")
        [date,machine,run_number,side,flowcell_key] = parse_sequencing_run_dir(path)
        index = mockdb["IndexReport"].__new__(system_config,flowcell_key=flowcell_key,output_dir=undetermined_dir)
        index.__fill_qsub_file__({"system": system_config,"pipeline": pipeline_config})
        index.__launch__(system_config)
        index.__finish__()
    for fastq_dir in fastq_output["fastqc"]:
        fastqc_dir = os.path.join(fastq_dir,"fastqc")
        if not os.path.isdir(fastqc_dir):
            os.makedirs(fastqc_dir)
        fastqc = mockdb["FastQC"].__new__(system_config,input_dir=fastq_dir,output_dir=fastqc_dir)
        fastqc.__fill_qsub_file__({"system": system_config,"pipeline": pipeline_config})
        fastqc.__launch__(system_config)
        fastqc.__finish__()
    for fastq_dir in fastq_output["md5"]:
        sample_key = re.sub("Sample_","",os.path.basename(fastq_dir))
        md5 = mockdb["Md5CheckSum"].__new__(system_config,input_dir=fastq_dir,sample_key=sample_key)
        md5.__fill_qsub_file__({"system": system_config,"pipeline": pipeline_config})
        md5.__launch__(system_config)
        md5.__finish__()

