OVERVIEW
    This application details the inheritance of objects, and returns lists of either ancestor classes (classes from which an object has inheritted behavior) or children classes (classes for which the object is an ancestor).  These lists are used heavily in the mockdb application.
