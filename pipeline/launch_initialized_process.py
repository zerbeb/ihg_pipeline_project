import sys
import argparse
from config.scripts import MyConfigParser
from mockdb.initiate_mockdb import initiate_mockdb,save_mockdb
from manage_storage.scripts import initiate_storage_devices, add_waiting_storage, add_running_storage
from processes.control import add_sequencing_run_object
from processes.control import maintain_sequencing_run_objects, advance_running_qc_pipelines
from processes.control import advance_running_std_pipelines,run_pipelines_with_enough_space
from processes.control import continue_seq_run, handle_automated_reports
from processes.transitions import things_to_do_if_initializing_pipeline_with_input_directory
from processes.transitions import things_to_do_if_initializing_flowcell_pipeline_with_input_directory


parser = argparse.ArgumentParser(description='Sometimes processes get stuck in the Initialized state.  This continues the process.')
parser.add_argument('process', help='This process to begin')
parser.add_argument('-d', '--debug', dest='debug', action='store_true', help='Turn debugging on', default=False)
parser.add_argument('--system_config', dest='system_config_file', help='The system configuration file', default='/home/sequencing/src/pipeline_project/pipeline/config/ihg_system.cfg')
options = parser.parse_args()
if options.debug is True:
    print "Options are " + str(options)

#Load configs
configs = {}
system_config = MyConfigParser()
system_config.read(options.system_config_file)
system_config.add_section("Logging")
if options.debug is True:
    system_config.set("Logging","debug","True")
else:
    system_config.set("Logging","debug","False")
configs.update({'system':system_config})

mockdb=initiate_mockdb(system_config)

try:
    state_dict = mockdb[options.process].__attribute_value_to_object_dict__('state')
    try:
        for process in state_dict['Initialized']:
            if configs['system'].get("Logging","debug") is "True":
                print "  Advancing:  "+str(process) 
            process.__launch__(configs['system'])
    except KeyError:
        print "No initialized processes of type " + options.process + " were found."
except KeyError:
    print "ERROR: The process " + options.process + " is not defined."

save_mockdb(configs['system'],mockdb)
