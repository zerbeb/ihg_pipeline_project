This is a quick application to find and replace entries in a file demarcated by FIELDBEGIN entry FIELDEND and return a string.  This is mainly used to fill in template qsub files that will be submitted via qsub to SGE.

This provides how much parallelization on SGE works.  Specifically, by assigning variables to different task ids, a single job can be easily paralized.  This is done through the interaction with the mockdb Qsub object in processes/models.py.
